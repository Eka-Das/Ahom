<h1 class="title text-center">¿Quién Soy?</h1>
	<center>
  		<img src="content/user/img/sanador.jpeg" class="card-img-top card-img-center w-75" alt="...">
	</center>
<div class="card mb-3">
  <div class="card-body">
    <h5 class="card-title">¿Quién Soy?</h5>
    <p class="card-text">¡Hola! Mi nombre es Alejandro Alonso (Brahma Hari Das).<br>

		Me considero un servidor.<br>
		Desde niño siento una inmensa atracción por lo espiritual. Mis padres nunca me impusieron ninguna religión, aunque si eran católicos.<br>
		Intenté acercarme a la iglesia, pero había algo dentro de mí, sentía que algo no cuadraba ahí, así que seguí buscando en diferentes lados, hasta que un día cansado, le pedí a esa fuerza infinita dentro de mí que por favor se revelara, que de verdad quería sentirlo, fue una petición de corazón, y semanas después conocí a mi maestro espiritual.<br>
		Cuando lo ví fue una conexión instantánea, supe inmediatamente que ahí encontraría eso que estaba buscando mi alma. Así fue, a los 14 años decidí entregarme por entero a este sendero espiritual, y descubrí dentro de mí el poder de la sanación muy presente siempre, gracias a que mi abuela (oaxaqueña) siempre nos curó con hierbas, siempre sentí atracción por eso, decidí hacerme masajista, me metí a un diplomado de Terapeuta Spa en Instituto Nordico aquí en Guadalajara, ahí aprendí distintas técnicas, mi maestro me enseño la sanación energética y espiritual, y su pareja, también mi maestra, me enseñó el arte del Reiki,  cuando ellos dejaron el cuerpo en 2017, dejaron en mí una profunda inspiración y responsabilidad de seguir sirviendo y ayudando a la humanidad, así que decidí continuar con todo lo aprendido, decidí estudiar Quiropraxia en Letfa Laha Zana y planeo estudiar Fisioterapia.</p>
    <!--<p class="card-text"><small class="text-muted">Última Actualización 24 De Mayo 2020</small></p>-->
    <p class="card-text"><small class="text-muted">Última Actualización 8 de Junio 2020</small></p>
  </div>
</div>




<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<div class="box">
    <div class="container">
     	<div class="row text-white">
			 
			    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
               
					<div class="box-part text-center">
                        <!--<a href="https://instagram.com/brahmaharidas?igshid=isl2ekdlz3hq">
                        </a>-->
                        	<i class="fa fa-instagram fa-3x" aria-hidden="true"></i>
                        
						<div class="title">
							<h4>Instagram</h4>
						</div>
                        <!--
						<div class="text">
							<span>Lorem ipsum dolor sit amet, id quo eruditi eloquentiam. Assum decore te sed. Elitr scripta ocurreret qui ad.</span>
						</div>-->
                        
						<a class="text-white" href="https://instagram.com/brahmaharidas?igshid=isl2ekdlz3hq">Seguir</a>
                        
					 </div>
				</div>	 
				<!--
				 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
               
					<div class="box-part text-center">
					    
					    <i class="fa fa-twitter fa-3x" aria-hidden="true"></i>
                    
						<div class="title">
							<h4>Twitter</h4>
						</div>
                        
						<div class="text">
							<span>Lorem ipsum dolor sit amet, id quo eruditi eloquentiam. Assum decore te sed. Elitr scripta ocurreret qui ad.</span>
						</div>
                        
						<a href="#">Learn More</a>
                        
					 </div>
				</div>	 
				-->
				 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
               
					<div class="box-part text-center">
                        
                        <i class="fa fa-facebook fa-3x" aria-hidden="true"></i>
                        
						<div class="title">
							<h4>Facebook</h4>
						</div>
                        <!--
						<div class="text">
							<span>Lorem ipsum dolor sit amet, id quo eruditi eloquentiam. Assum decore te sed. Elitr scripta ocurreret qui ad.</span>
						</div>
                        -->
						<a class="text-white" href="https://www.facebook.com/AhomSanarte/">Like!</a>
                        
					 </div>
				</div>	 
				
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
               
					<div class="box-part text-center">
                        
                        <i class="fa fa-youtube fa-3x" aria-hidden="true"></i>
                        
						<div class="title">
							<h4>YouTube</h4>
						</div>
                        <!--
						<div class="text">
							<span>Lorem ipsum dolor sit amet, id quo eruditi eloquentiam. Assum decore te sed. Elitr scripta ocurreret qui ad.</span>
						</div>
                        -->
						<a class="text-white" href="https://www.youtube.com/channel/UCYdTuVMO5FIPiJpolTxEFqg">Visitar</a>
                        
					 </div>
				</div>	 
				<!--
				 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
               
					<div class="box-part text-center">
					    
					    <i class="fa fa-google-plus fa-3x" aria-hidden="true"></i>
                    
						<div class="title">
							<h4>Google</h4>
						</div>
                        
						<div class="text">
							<span>Lorem ipsum dolor sit amet, id quo eruditi eloquentiam. Assum decore te sed. Elitr scripta ocurreret qui ad.</span>
						</div>
                        
						<a href="#">Learn More</a>
                        
					 </div>
				</div>	 
				
				 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
               
					<div class="box-part text-center">
                        
                        <i class="fa fa-github fa-3x" aria-hidden="true"></i>
                        
						<div class="title">
							<h4>Github</h4>
						</div>
                        
						<div class="text">
							<span>Lorem ipsum dolor sit amet, id quo eruditi eloquentiam. Assum decore te sed. Elitr scripta ocurreret qui ad.</span>
						</div>
                        
						<a href="#">Learn More</a>
                        
					 </div>
				</div>-->
		
		</div>		
    </div>
</div>