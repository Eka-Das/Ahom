<h1 class="text-center">Conseguir Curso</h1>

<!-- 

	
	$100 Mxn por clase (el curso tendrá 21 clases)
	$1500 mxn todo el curso
	$1000 mxn pagando por anticipado
	Pago por tarjeta o Paypal

-->

<div class="card-deck">
	<div class="card">
    <img src="" class="card-img-top" alt="">
    <div class="card-body">
      <h5 class="card-title">Curso completo</h5>
      <p class="card-text">Todos los beneficios de este curso a partir del día de inicio.<br>$1500</p>
      <!-- Button trigger modal -->
		<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
		  Conseguir
		</button>
      <p class="card-text"><small class="text-muted"></small></p>
    </div>
  </div>

  <div class="card">
    <img src="" class="card-img-top" alt="">
    <div class="card-body">
      <h5 class="card-title">Descuento</h5>
      <p class="card-text">Adquiere el curso completo con un descuento por pago anticipado,antes del inicio del curso.<br>$1000</p>
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
		  Conseguir
		</button>
      <p class="card-text"><small class="text-muted"></small></p>
    </div>
  </div>
  
  <div class="card">
    <img src="" class="card-img-top" alt="">
    <div class="card-body">
      <h5 class="card-title">Pago por clase</h5>
      <p class="card-text">Por clase (el curso tendrá 21 clases).<br>$100</p>
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
		  Conseguir
		</button>
      <p class="card-text"><small class="text-muted"></small></p>
    </div>
  </div>
</div>	
<br>
<br>




<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Métodos De Pago</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	    	<div class="card">
			    <img src="" class="card-img-top" alt="">
			    <div class="card-body">
			      <h5 class="card-title">Depósito o transferencia</h5>
			      <p class="card-text">BBVA Bancomer<br> 4152 3136 3574 9273</p>
			      <p class="card-text"><small class="text-muted"></small></p>
			    </div>
			  </div>

		  <div class="card">
		    <img src="" class="card-img-top" alt="">
		    <div class="card-body">
		      <h5 class="card-title"><i class="fab fa-paypal"></i>PayPal</h5>
		      <p class="card-text"><a href="paypal.me/ahomsanarte.">paypal.me/ahomsanarte.</a> </p>
		      <p class="card-text"><small class="text-muted"></small></p>
		    </div>
		  </div>
<!--
		  <div class="card">
		    <img src="" class="card-img-top" alt="">
		    <div class="card-body">
		      <h5 class="card-title">Tarjeta de crédito o débito</h5>
		      <p class="card-text">$1500</p>
		      <p class="card-text"><small class="text-muted"></small></p>
		    </div>
		  </div>
		  -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
      </div>
    </div>
  </div>
</div>


