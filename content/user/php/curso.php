<div class="card text-center">
  <div class="card-header">
    Curso Online
  </div>
  <div class="card-body">
    <h5 class="card-title"><b>Crecimiento Espiritual</b></h5>
    <a href="#" class="badge badge-secondary">Inicia el 13 de Junio 2020</a>
    <p class="card-text text-left">
    	<b>Objetivos: </b>
    	<ul class="text-left">
    		<li>Llegar a quien tengamos que llegar. </li>
    		<li>Compartirte información, técnicas y ejercicios para que crezcas espiritualmente más fácilmente y con mayor jubilo</li>
    		<li>Demostrarte que el crecimiento espiritual es un camino de júbilo y no necesariamente de lucha</li>
    		<li>Lograr que puedas experimentar tú mismo tu divinidad, y estados elevados de consciencia.</li>
    		<li>Darte las herramientas para hacer que tu vida diaria funcione y para traer niveles de orden, armonía, claridad y amor cada vez más altos a cada aspecto de tu vida.</li>
    	</ul>
	</p>
	<p class="card-text text-left">
		<b>Método: </b><br>
		Haremos un grupo privado en Facebook para que la clase quede accesible a quien quiera volver a verla cuantas veces desee. <br>

		Será una clase por semana, la cual incluye una charla, meditación guiada, teoría - práctica, y sección de preguntas y respuestas.
		<br>
	</p>

	<div class="accordion" id="accordionExample">
	  <div class="card">
	    <div class="card-header" id="headingOne">
	      <h2 class="mb-0">
	        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
	          <b>Contenido</b>
	        </button>
	      </h2>
	    </div>

	    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
	      <div class="card-body text-left">
	      
			<b>Primera sección: El Ascenso</b> <br>
			<ol>
				<li>Ser tu Ser Superior</li>
				<li>Crear con la luz</li>
				<li>Cómo unirse con la Mente Universal</li>
				<li>Unión con la voluntad superior</li>
				<li>Ver el cuadro más grande: Recibir revelaciones</li>
				<li>Apertura de la consciencia de los planos internos</li>
				<li>Ascenso a la consciencia superior</li>
			</ol>

			<b>Segunda sección: Apertura Interna</b><br>
			<ol>
				<li>Como elevar tu vibración</li>
				<li>Como calmar tus emociones</li>
				<li>Permite tu bien superior</li>
				<li>Aceleración de tu crecimiento</li>
				<li>La elección de tu realidad: Creación de futuros probables</li>
				<li>Atravesamiento del vacío</li>
				<li>Cómo expandir y contraer el tiempo</li>
			</ol>

			<b>Tercera Sección: Expansión al exterior</b><br>
			<ol>
				<li>Cómo convertirte en fuente de luz</li>
				<li>Iluminación a través del servicio</li>
				<li>Levantar los velos de la ilusión</li>
				<li>Cómo comunicarte como tu ser superior</li>
				<li>El uso correcto de la Voluntad</li>
				<li>El Desapego</li>
				<li>Cómo hacerte transparente</li>

			</ol>
	      </div>
	    </div>
	  </div>
  	</div>

	<br>

	<p class="card-text text-left">
		<b>Intercambio de energía:</b><br>
		Con la intención de que sea más accesible daremos la posibilidad de que escojas de qué manera quieres aportar.<br><br>

		$100 Mxn por clase (el curso tendrá 21 clases)<br>
		$1500 mxn todo el curso<br>
		$1000 mxn pagando por anticipado<br>
		Pago por tarjeta o Paypal<br>
	</p>

	<p class="card-text text-left">
		Uno de nuestros lemas es: 
	</p>

	<blockquote class="blockquote">
	  <p class="mb-0">Que la moneda no te detenga, si realmente deseas tomar el curso, pero no está en tus 		posibilidades económicas, hablemos personalmente para encontrar una solución, estamos abiertos al 		trueque y más, el caso es hacer un intercambio de energia.</p>
	</blockquote>
    <a href="?action=comenzar" class="btn btn-primary">¡Quiero Empezar!</a>

  </div>

  <div class="card-footer text-muted">
    Om Tat Sat
  </div>

</div>









