<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand" href="#">
    <img src="content/user/img/ahom.png" width="30" height="30" class="d-inline-block align-top" alt="" loading="lazy">
    Ahom
  </a>

  <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
      <li class="nav-item active">
        <a class="nav-link" href="?action=home">Inicio <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="?action=sorteo">¡Sorteo!</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="?action=curso">Curso</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Terapias
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Quiropraxia</a>
          <a class="dropdown-item" href="#">Masajes</a>
          <a class="dropdown-item" href="#">Reiki</a>
          <a class="dropdown-item" href="#">Acupuntura tibetana</a>
          <a class="dropdown-item" href="#">Reflexologia podal</a>
          <div class="dropdown-divider"></div>
           <a class="dropdown-item" href="#">Plantas maestras</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Lecturas de tarot</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="?action=about" tabindex="-1" aria-disabled="true">¿Quién Soy?</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="?action=contacto" tabindex="-1" aria-disabled="true">Contacto</a>
      </li>
    </ul>
    <!--
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>-->
  </div>
</nav>