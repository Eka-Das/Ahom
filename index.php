<!DOCTYPE html>
<html lang="es" dir="ltr">
<!--ultima edicion 24 de may 2020-->
<!--ultima edicion 30 de may 2020-->

  <head>
    <meta charset="utf-8">
    <title>Ahom Sanarte</title>
    <?php include 'content/user/php/head.php'; ?>

  </head>
  <body>

    <div class="container">

      <?php
        include 'content/user/php/navbar.php';

        if (isset($_GET['action'])) {
          $action = $_GET['action'];
        }else {
          $action = 'home';
        }
        switch ($action) {
          case 'home':
            include 'content/user/php/home.php';
          break;

          case 'construyendo':
            include 'content/user/php/construyendo.php';
          break;

          case 'curso':
            include 'content/user/php/curso.php';
          break;

          case 'about':
            include 'content/user/php/about.php';
          break;

          case 'contacto':
            include 'content/user/php/contacto.php';
          break;  

          case 'comenzar':
            include 'content/user/php/comenzar.php';
          break;

          case 'sorteo':
            include 'content/user/php/sorteo.php';
          break;

          default:
          break;
        }
        
        include 'content/user/php/footer.php';
      ?>
    </div>

  </body>
</html>
